# 安装依赖
npm install

# 建议不要用 cnpm 安装 会有各种诡异的bug 可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 本地开发 启动项目
npm run dev

# bug
https://github.com/PanJiaChen/vue-element-admin/issues

# 性能分析
npm run preview -- --report
