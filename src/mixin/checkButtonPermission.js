import store from '@/store'
export default {
  methods: {
    //该方法采用mixin全局混入的形式、注册到全局所有的组件中、
    //全局的组件都可以使用该方法：传入的参数为要检查的key：例如：add- user
    checkPermission(key) {
      alert(123)
      const { userInfo } = store.state.user
      if (userInfo.roles.points && userInfo.roles.points.length) {
        return userInfo.roles.points.some(item => item === key)
      }
      return false
    }
  }
}