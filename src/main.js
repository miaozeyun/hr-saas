import Vue from 'vue'
import 'normalize.css/normalize.css' // A modern alternative to CSS resets
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n
import '@/styles/index.scss' // global css
import i18n from '@/lang'


import checkButtonPermission from '@/mixin/checkButtonPermission' //全局混入检查按钮权限
import App from './App'
import store from './store'
import router from './router'
import component from '@/components/index'  //导入全局自定义组件
import '@/icons' // icon
import '@/permission' // permission control

Vue.mixin(checkButtonPermission) //挂载全局混入到vue实例身上
Vue.use(ElementUI, { locale })
Vue.config.productionTip = false
Vue.use(component) //注册全局自定义组件、注册完成后即可无需导入、全局使用。


// 设置element为当前的语言
Vue.use(ElementUI, {
  i18n: (key, value) => i18n.t(key, value)
})

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})
