import request from '@/utils/request'

export function login(data) {
  return request({
    url: 'iext/back/AuthController/login', // 因为所有的接口都要跨域 表示所有的接口要带 /api
    method: 'post',
    data
  })
}


export function getPublic() {
  return request({
    url: '/iext/back/AuthController/getPubKey',
    method: 'post'
  })
}



export function getInfo(token) {
  return request({
    url: '/sys/profile',
    method: 'post'
  })
}

export function logout() { }

export function getUserDetailById(id) {
  return request({
    url: `/sys/user/${id}`
  })
}