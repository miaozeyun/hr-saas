// vuex中的数据进行持久化处理
import { getToken, setToken, removeToken } from '@/utils/auth'
import { getPublic, login, getInfo, getUserDetailById } from '@/api/user'
import { resetRouter } from '@/router/index'
const state = {
  token: getToken(),
  userInfo: {}
}
const mutations = {
  setToken_muta(state, token) {
    state.token = token
    // 将token持久化
    setToken(token)
  },
  //登出
  removeToken_muta(state) {
    state.token = null
    removeToken()
    state.userInfo = {}
  },
  setUserInfo_muta(state, data) {
    state.userInfo = data
  },

}
const actions = {
  async login_action(context, data) {
    let strPublic = await getPublic()
    let keyPassword = sm2Encrypt(data.password, strPublic, 0); // 加密结果
    let obj = {
      phone: data.phone,
      password: keyPassword,
    }
    const res = await login(obj)
    context.commit('setToken_muta', res.token)
    context.commit('setUserInfo_muta', res)
    return res
  },
  async getUserInfo_action(context) {
    const result = await getInfo()  // 获取返回值
    const baseInfo = await getUserDetailById(result.userId) // 为了获取头像
    const baseResult = { ...result, ...baseInfo }
    context.commit('setUserInfo_muta', baseResult) // 将整个的个人信息设置到用户的vuex数据中
    return baseResult // 这里为什么要返回 为后面埋下伏笔
  },

  removeToken_muta(context) {
    context.commit('removeToken_muta')
    // 重置路由
    resetRouter()
    // 还有一步  vuex中的数据是不是还在
    // 要清空permission模块下的state数据
    // vuex中 user子模块  permission子模块
    // 子模块调用子模块的action  默认情况下 子模块的context是子模块的
    // 父模块 调用 子模块的action
    context.commit('permission/setRoutes', [], { root: true })
    // 子模块调用子模块的action 可以 将 commit的第三个参数 设置成  { root: true } 就表示当前的context不是子模块了 而是父模块

  }

}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
