const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.userInfo.staffPhoto,
  userID: state => state.user.userInfo.id,
  name: state => state.user.userInfo.companyName, // 建立用户名称的映射
  routes: state => state.permission.routes // 导出当前的路由
}
export default getters
