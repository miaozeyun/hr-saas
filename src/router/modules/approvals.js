// 导出路由规则
import Layout from '@/layout'
//  {  path: '', component: '' }
// 每个子模块 其实 都是外层是layout  组件位于layout的二级路由里面
export default {
    path: '/approvals', // 路径
    name: 'approvals', // 给路由规则加一个name
    component: Layout, // 组件
    meta: {
        title: '审批管理', // meta属性的里面的属性 随意定义 但是这里为什么要用title呢， 因为左侧导航会读取我们的路由里的meta里面的title作为显示菜单名称
        icon: 'el-icon-eleme'
    },
    children: [
        {
            path: 'people',//三级
            name: 'people',
            component: () => import('@/views/approvals/people'),
            meta: { title: '人员审批管理', icon: 'point' },
        },
        {
            path: 'peopleDetail',//三级
            name: 'peopleDetail',
            component: () => import('@/views/approvals/people/detail.vue'),
            meta: { title: '人员审批管理', icon: 'point' },
            hidden: true
        },
        {
            path: 'company',// 三级
            name: 'company',
            component: () => import('@/views/approvals/company'),
            meta: { title: '请假审批管理', icon: 'point' }
        }
    ]
}

// 当你的访问地址 是 /employees的时候 layout组件会显示 此时 你的二级路由的默认组件  也会显示