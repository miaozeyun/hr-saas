// 该文件负责所有的公共的组件的全局注册   Vue.use
import ScreenFull from './ScreenFull'
import ThemePicker from './ThemePicker'
import TagsView from './TagsView'
import lang from './lang'

export default {
    install(Vue) {
        //  注册全局的通用栏组件对象
        Vue.component('m-screenFull', ScreenFull)
        Vue.component('m-themePicker', ThemePicker)
        Vue.component('m-tagsView', TagsView)
        Vue.component('m-lang', lang)
    }
}