import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
import router from '@/router'
const server = axios.create(
    {
        baseURL: process.env.VUE_APP_BASE_API, // 设置axios请求的基础的基础地址
        timeout: 10000 // 定义10秒超时
    }
)
server.interceptors.request.use(
    // config是请求的配置信息
    config => {
        if (store.getters.token) {
            config.headers['Authorization'] = `Bearer ${store.getters.token}`
        }
        return config // 必须是要返回的
    }, error => {
        return Promise.reject(error)
    }
)
server.interceptors.response.use(response => {
    const { success, msg, body } = response.data // 结构数据
    // 判断是否为成功执行
    if (success == 1) {
        // return Promise.resolve(data)
        return body
    } else {
        // 业务已经错误了 还能进then ? 不能 ！ 应该进catch
        Message.error(msg) // 提示错误信息
        return Promise.reject(new Error(msg))
    }
}, error => {
    if (error && error.response && error.response.data.code === 10002) {
        // 当状态为10002时、后端告诉我超时了
        store.commit('user/removeToken_muta') // 调用登出、清理token
        router.push('/login')
        return Promise.reject(new Error('超时了'))
    } else {
        // 失败处理
        Message.error(error) // 提示错误信息
        return Promise.reject(error) // 返回错误信息、让当前1执行联直接跳过成功、进入catch
    }
})
export default server
